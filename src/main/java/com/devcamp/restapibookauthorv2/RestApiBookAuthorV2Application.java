package com.devcamp.restapibookauthorv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiBookAuthorV2Application {

	public static void main(String[] args) {
		SpringApplication.run(RestApiBookAuthorV2Application.class, args);
	}

}
